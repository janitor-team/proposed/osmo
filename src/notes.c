
/*
 * Osmo - a handy personal organizer
 *
 * Copyright (C) 2007 Tomasz Maka <pasp@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "about.h"
#include "backup.h"
#include "notes.h"
#include "i18n.h"
#include "calendar.h"
#include "calendar_notes.h"
#include "options_prefs.h"
#include "preferences_gui.h"
#include "notes_items.h"
#include "calendar_utils.h"
#include "utils.h"
#include "utils_date.h"
#include "utils_date_time.h"
#include "utils_gui.h"
#include "stock_icons.h"
#include "gtksourceiter.h"

#ifdef NOTES_ENABLED

#ifdef HAVE_LIBGRINGOTTS
#include <libgringotts.h>
#endif /* HAVE_LIBGRINGOTTS */

/*============================================================================*/

static void
show_preferences_window_cb(GtkToolButton *toolbutton, gpointer data) {
    GUI *appGUI = (GUI *) data;
    appGUI->opt->window = opt_create_preferences_window(appGUI);
    gtk_widget_show(appGUI->opt->window);

    gint page = gtk_notebook_page_num(GTK_NOTEBOOK(appGUI->opt->notebook), appGUI->opt->notes);
    gtk_notebook_set_current_page(GTK_NOTEBOOK(appGUI->opt->notebook), page);
}

/*------------------------------------------------------------------------------*/

void
notes_show_selector_editor (gint mode, GUI *appGUI) {

GtkTextBuffer *buffer;
GtkTextMark *cursor;
GtkTextIter iter;
GtkTreeIter n_iter;
GtkTreePath *path, *sort_path, *filter_path;
gchar *name, *fontname;
gint line;
gboolean remember_cursor, readonly = FALSE;
gchar tmpbuf[BUFFER_SIZE];

	if (mode == SELECTOR) {

		gtk_widget_show (appGUI->nte->vbox_selector);
		gtk_widget_hide (appGUI->nte->vbox_editor);
		gtk_widget_grab_focus (appGUI->nte->notes_find_entry);
		appGUI->nte->editor_active = FALSE;

	} else if (mode == EDITOR) {
 
		appGUI->nte->htmlViewPosition = 0;

		gtk_widget_hide (appGUI->nte->find_hbox);
		gtk_widget_hide (appGUI->nte->vbox_selector);
		gtk_widget_show (appGUI->nte->vbox_editor);
		gtk_entry_set_text(GTK_ENTRY(appGUI->nte->find_entry), "");
		gtk_widget_grab_focus (appGUI->nte->editor_textview);
		appGUI->nte->editor_active = TRUE;

		gtk_tree_view_get_cursor (GTK_TREE_VIEW (appGUI->nte->notes_list), &sort_path, NULL);

		if (sort_path == NULL) return;
		filter_path = gtk_tree_model_sort_convert_path_to_child_path (GTK_TREE_MODEL_SORT(appGUI->nte->notes_sort), sort_path);
		gtk_tree_path_free (sort_path);

		if (filter_path == NULL) return;
		path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter), filter_path);
		gtk_tree_path_free (filter_path);

		gtk_tree_model_get_iter (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &n_iter, path);
		gtk_tree_model_get (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &n_iter,
                            N_COLUMN_NAME, &name,
                            N_COLUMN_EDITOR_LINE, &line,
							N_COLUMN_REMEMBER_EDITOR_LINE, &remember_cursor, 
							N_COLUMN_EDITOR_READONLY, &readonly,
							N_COLUMN_FONTNAME, &fontname, -1);
		g_snprintf(tmpbuf, BUFFER_SIZE, "<big><i><b>%s</b></i></big>", name);
		gtk_label_set_markup (GTK_LABEL (appGUI->nte->title_label), tmpbuf);
        appGUI->nte->fd_notes_font = pango_font_description_from_string(fontname);
	    gtk_widget_override_font (GTK_WIDGET(appGUI->nte->editor_textview), appGUI->nte->fd_notes_font);
		gtk_font_button_set_font_name (GTK_FONT_BUTTON(appGUI->nte->font_picker), fontname);
		g_free(name);
		g_free(fontname);
		gtk_tree_path_free (path);

		/* restore cursor position */

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

		if (remember_cursor == TRUE) {
			gtk_text_buffer_get_iter_at_line (GTK_TEXT_BUFFER (buffer), &iter, line);
			gtk_text_buffer_place_cursor (GTK_TEXT_BUFFER (buffer), &iter);
			cursor = gtk_text_buffer_get_mark (buffer, "insert");
			gtk_text_view_scroll_to_mark (GTK_TEXT_VIEW (appGUI->nte->editor_textview), cursor, 0.0, TRUE, 0.0, 0.0);
		} else {
			gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buffer), &iter);
			gtk_text_buffer_place_cursor (GTK_TEXT_BUFFER (buffer), &iter);
			gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (appGUI->nte->editor_textview), &iter, 0.0, FALSE, 0.0, 0.0);
		}

		/* read-only */
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (appGUI->nte->readonly_checkbutton), readonly);
		gtk_text_view_set_editable (GTK_TEXT_VIEW (appGUI->nte->editor_textview), !readonly);
		gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (appGUI->nte->editor_textview), !readonly);
		gtk_widget_set_sensitive (appGUI->nte->font_picker, !readonly);
		appGUI->nte->note_read_only = readonly;

#ifdef HAVE_LIBWEBKIT
		update_readonly_view (appGUI);
#endif  /* HAVE_LIBWEBKIT */

    }
}

/*------------------------------------------------------------------------------*/

void
editor_save_buffer_cb (GtkToolButton *toolbutton, gpointer data) {

GtkTextBuffer *buffer;
guchar *text;
guint32 current_date;
gint current_time;
GtkTreeIter iter, filter_iter, sort_iter;
GtkTreeModel *model;

	GUI *appGUI = (GUI *)data;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

	text = (unsigned char*) utl_gui_text_buffer_get_text_with_tags (buffer);

	if (appGUI->nte->encrypted == TRUE) {
#ifdef HAVE_LIBGRINGOTTS
            if (appGUI->nte->keyholder != NULL && appGUI->nte->context != NULL && appGUI->nte->filename != NULL) {
                grg_encrypt_file(appGUI->nte->context, appGUI->nte->keyholder,
                        (unsigned char*) notes_get_full_filename(appGUI->nte->filename, appGUI),
                        (guchar *) text, -1);
                grg_free(appGUI->nte->context, text, -1);
            } else {
                g_free(text);
            }
#endif /* HAVE_LIBGRINGOTTS */
	} else {
            g_file_set_contents (notes_get_full_filename(appGUI->nte->filename, appGUI), 
                                                     (gchar *) text, strlen((gchar *) text), NULL);
            g_free(text);
	}

	appGUI->nte->changed = FALSE;
	gtk_widget_set_sensitive (GTK_WIDGET(appGUI->nte->save_button), FALSE);

        sort_iter = utl_gui_get_first_selection_iter(appGUI->nte->notes_list_selection, &model);
        gtk_tree_model_sort_convert_iter_to_child_iter(GTK_TREE_MODEL_SORT(appGUI->nte->notes_sort), &filter_iter, &sort_iter);
        gtk_tree_model_filter_convert_iter_to_child_iter(GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter), &iter, &filter_iter);

        current_date = utl_date_get_current_julian();
        current_time = utl_time_get_current_seconds();



        gtk_list_store_set(appGUI->nte->notes_list_store, &iter,
                N_COLUMN_LAST_CHANGES_DATE, get_date_time_str(current_date, current_time),
                N_COLUMN_LAST_CHANGES_DATE_JULIAN, current_date,
                N_COLUMN_LAST_CHANGES_TIME, current_time, -1);
}

/*------------------------------------------------------------------------------*/

void
editor_find_text_show_cb (GtkToolButton *toolbutton, gpointer data) {

	GUI *appGUI = (GUI *)data;

	gtk_widget_show (appGUI->nte->find_hbox);
	gtk_widget_grab_focus (appGUI->nte->find_entry);

	appGUI->nte->find_hbox_visible = TRUE;
}

/*------------------------------------------------------------------------------*/

void
editor_find_text_hide_cb (GtkWidget *widget, gpointer user_data) {

	GUI *appGUI = (GUI *)user_data;

	utl_gui_change_bg_widget_state (appGUI->nte->find_entry, NULL, appGUI);

	gtk_widget_hide (appGUI->nte->find_hbox);
	gtk_widget_grab_focus (appGUI->nte->editor_textview);

	appGUI->nte->find_hbox_visible = FALSE;
}

/*------------------------------------------------------------------------------*/

void
editor_close_cb (GtkToolButton *toolbutton, gpointer data) {

GtkTextBuffer *buffer;
GtkTextIter iter_s, iter_e, l_iter;
GtkTreeIter n_iter;
GtkTreePath *path, *sort_path, *filter_path;
gint response, line;
GtkTextMark *cursor;
gchar tmpbuf[BUFFER_SIZE];

	GUI *appGUI = (GUI *)data;

	if (appGUI->nte->changed == TRUE) {

		g_snprintf(tmpbuf, BUFFER_SIZE, "%s\n\n%s", _("The note has changed."), _("Do you want to save it?"));

		response = utl_gui_create_dialog (GTK_MESSAGE_QUESTION, tmpbuf, GTK_WINDOW(appGUI->main_window));

		if (response == GTK_RESPONSE_YES) {
			editor_save_buffer_cb (NULL, appGUI);
		} else if (response != GTK_RESPONSE_NO) {
			return;
		}
	}

#ifdef HAVE_GSPELL
	gtk_toggle_tool_button_set_active (GTK_TOGGLE_TOOL_BUTTON (appGUI->nte->spell_check_button), FALSE);
#endif /* HAVE_GSPELL */

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

#ifdef HAVE_LIBGRINGOTTS
	if (appGUI->nte->keyholder != NULL && appGUI->nte->context != NULL) {
		GtkClipboard *clipboard;
		grg_key_free (appGUI->nte->context, appGUI->nte->keyholder);
		appGUI->nte->keyholder = NULL;
		grg_context_free (appGUI->nte->context);
		appGUI->nte->context = NULL;

		/* clear clipboards for encrypted notes */
		clipboard = gtk_clipboard_get(GDK_SELECTION_PRIMARY);                                                            
		gtk_clipboard_clear(clipboard);                                                                                  
		gtk_clipboard_set_text(clipboard, "", 0);                                                                        

		clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);                                                          
		gtk_clipboard_clear(clipboard);                                                                                
		gtk_clipboard_set_text(clipboard, "", 0);
	}
#endif /* HAVE_LIBGRINGOTTS */

	if (appGUI->nte->filename != NULL) {
		g_free(appGUI->nte->filename);
		appGUI->nte->filename = NULL;
	}

	cursor = gtk_text_buffer_get_mark (buffer, "insert");
	gtk_text_buffer_get_iter_at_mark (buffer, &l_iter, cursor);
	line = gtk_text_iter_get_line (&l_iter);

	gtk_text_buffer_get_bounds(buffer, &iter_s, &iter_e);
	gtk_text_buffer_delete(buffer, &iter_s, &iter_e);
	appGUI->nte->buffer_check_modify_enable = FALSE;

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (appGUI->nte->notes_list), &sort_path, NULL);

	if (sort_path == NULL) return;
	filter_path = gtk_tree_model_sort_convert_path_to_child_path (GTK_TREE_MODEL_SORT(appGUI->nte->notes_sort), sort_path);
	gtk_tree_path_free (sort_path);

	if (filter_path == NULL) return;
	path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter), filter_path);
	gtk_tree_path_free (filter_path);

	gtk_tree_model_get_iter (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &n_iter, path);
	gtk_list_store_set (appGUI->nte->notes_list_store, &n_iter,
						N_COLUMN_EDITOR_LINE, line, 
						N_COLUMN_EDITOR_READONLY, gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (appGUI->nte->readonly_checkbutton)),
						N_COLUMN_FONTNAME, gtk_font_button_get_font_name (GTK_FONT_BUTTON(appGUI->nte->font_picker)), -1);
	gtk_tree_path_free (path);

	notes_show_selector_editor (SELECTOR, appGUI);

	gtk_widget_grab_focus (appGUI->nte->notes_list);
}

/*------------------------------------------------------------------------------*/

static void
set_text_attribute_cb (GtkToolButton *toolbutton, gpointer data) {

GtkTextBuffer *buffer;
gchar *tagname;

	GUI *appGUI = (GUI *)data;

	tagname = (gchar*) g_object_get_data (G_OBJECT (toolbutton), "tag");
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));
	utl_gui_text_buffer_toggle_tags (buffer, tagname);
	g_signal_emit_by_name(G_OBJECT(buffer), "changed");
}

/*------------------------------------------------------------------------------*/

void
clear_text_attributes_cb (GtkToolButton *toolbutton, gpointer data) {

GtkTextIter start, end;
GtkTextBuffer *buffer;

	GUI *appGUI = (GUI *)data;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
	gtk_text_buffer_remove_all_tags (buffer, &start, &end);
}

/*------------------------------------------------------------------------------*/

void
open_url_links_cb (GtkToolButton *toolbutton, gpointer data) {

GtkTextIter start, end;
GtkTextBuffer *buffer;
GtkWidget *info_dialog;
gchar *text;
gint n, m, response = -1;
gchar tmpbuf[BUFFER_SIZE];

	GUI *appGUI = (GUI *)data;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
        text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &start, &end, FALSE);

	n = utl_open_count_url_links (text, TRUE);

	if (n == 0) {

		utl_gui_create_dialog (GTK_MESSAGE_INFO, _("No URLs found in the selection..."), 
							   GTK_WINDOW(appGUI->main_window));

	} else if (n == 1) {

		utl_open_count_url_links (text, FALSE);

	} else {
		
		g_snprintf (tmpbuf, BUFFER_SIZE, "%d %s\n\n%s", n,
					_("URLs will be opened in the default browser."),
					_("Do you want to continue?"));

		info_dialog = gtk_message_dialog_new_with_markup (GTK_WINDOW(appGUI->main_window), GTK_DIALOG_MODAL,
											  GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, tmpbuf, NULL);

		gtk_window_set_title(GTK_WINDOW(info_dialog), _("Question"));
		gtk_window_set_position(GTK_WINDOW(info_dialog), GTK_WIN_POS_CENTER_ON_PARENT);
		gtk_widget_show (info_dialog);

		response = gtk_dialog_run(GTK_DIALOG(info_dialog));
		gtk_widget_destroy(info_dialog);

		if (response == GTK_RESPONSE_YES) {
			m = utl_open_count_url_links (text, FALSE);
			if (n != m) {
				g_snprintf (tmpbuf, BUFFER_SIZE, "%s\n\n(%d / %d %s)", 
							_("Too many links selected!"), m, n, 
							_("links opened"));
				utl_gui_create_dialog (GTK_MESSAGE_INFO, tmpbuf, 
									   GTK_WINDOW(appGUI->main_window));
			}
		}
	}
        g_free(text);
}

/*------------------------------------------------------------------------------*/

static void
notes_add_item_cb (GtkToolButton *toolbutton, gpointer data) {

    GUI *appGUI = (GUI *)data;
    notes_add_entry (appGUI);
}

/*------------------------------------------------------------------------------*/

static void
notes_edit_item_cb (GtkToolButton *toolbutton, gpointer data) {

    GUI *appGUI = (GUI *)data;
    notes_edit_dialog_show(appGUI->nte->notes_list, appGUI->nte->notes_filter, appGUI);
}

/*------------------------------------------------------------------------------*/

static void
notes_delete_item_cb (GtkToolButton *toolbutton, gpointer data) {

    GUI *appGUI = (GUI *)data;
    notes_remove_dialog_show(appGUI);
}

/*------------------------------------------------------------------------------*/

void
insert_current_date_and_time_cb (GtkToolButton *toolbutton, gpointer data)
{
	time_t tmm;
	gchar *datestr;
        GUI *appGUI = (GUI *)data;
        
	if (config.use_system_date_in_notes == TRUE) {
		tmm = time (NULL);
		gtk_text_buffer_insert_at_cursor (gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview)), 
		                                  asctime (localtime (&tmm)), -1);
	} else {
		datestr = utl_date_time_print_default (utl_date_get_current_julian (), utl_time_get_current_seconds (), FALSE);
		gtk_text_buffer_insert_at_cursor (gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview)), 
		                                  datestr, -1);
		g_free (datestr);
	}
}

/*------------------------------------------------------------------------------*/

void
insert_separator_cb (GtkToolButton *toolbutton, gpointer data) {

gint chars, i;
gchar tmpbuf[BUFFER_SIZE];
PangoLayout *layout;
PangoRectangle logical_rect;
GtkWidget *vscrollbar;

    GUI *appGUI = (GUI *)data;

    memset (tmpbuf, 0, BUFFER_SIZE);

    tmpbuf[0] = config.text_separator;
    layout = gtk_widget_create_pango_layout (appGUI->nte->editor_textview, NULL);
    pango_layout_set_font_description (layout, appGUI->nte->fd_notes_font);
    pango_layout_set_text (layout, tmpbuf, -1);
    pango_layout_get_pixel_extents (layout, NULL, &logical_rect);

    vscrollbar = gtk_scrolled_window_get_vscrollbar (GTK_SCROLLED_WINDOW (appGUI->nte->editor_scrolledwindow));

	if (gtk_widget_get_visible(vscrollbar) == TRUE) {
        chars = (gtk_widget_get_allocated_width(appGUI->nte->editor_textview)) / logical_rect.width;
    } else {
        chars = (gtk_widget_get_allocated_width(appGUI->nte->editor_textview) - utl_gui_get_sw_vscrollbar_width (appGUI->nte->editor_scrolledwindow)) / logical_rect.width;
    }
    chars = (chars > BUFFER_SIZE) ? BUFFER_SIZE - 2 : chars - 2;

    g_object_unref (G_OBJECT(layout));

    i = 0;
    tmpbuf[i++] = '\n';
    while (i < chars) tmpbuf[i++] = config.text_separator;
    tmpbuf[i++] = '\n';

    gtk_text_buffer_insert_at_cursor (gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview)), 
                                      tmpbuf, -1);
}

/*------------------------------------------------------------------------------*/

gchar *
get_buffer_info_string (GtkTextBuffer *buffer, gboolean selection) 
{
	GtkTextIter start_iter, end_iter;
	gchar tmpbuf[BUFFER_SIZE];
	gchar *text;
	PangoLogAttr *attrs;
	gint i, words, lines, chars, white_chars, bytes;

	if (selection == TRUE && gtk_text_buffer_get_has_selection (buffer) == FALSE)
		return g_strdup("");

    if (selection == TRUE && gtk_text_buffer_get_has_selection (buffer) == TRUE) {
        gtk_text_buffer_get_selection_bounds (buffer, &start_iter, &end_iter);
    } else {
        gtk_text_buffer_get_start_iter (buffer, &start_iter);
        gtk_text_buffer_get_end_iter (buffer, &end_iter);
    }

    text = gtk_text_buffer_get_text (GTK_TEXT_BUFFER (buffer), &start_iter, &end_iter, FALSE);
    if (text == NULL) return g_strdup("");

    words = 0;
    white_chars = 0;
    lines = gtk_text_buffer_get_line_count (buffer);
    bytes = strlen (text);
    chars = g_utf8_strlen (text, -1);

    attrs = g_new0 (PangoLogAttr, chars + 1);   /* based on code by Paolo Maggi */
    pango_get_log_attrs (text, -1, 0, pango_language_from_string ("C"), attrs, chars + 1);

    for (i=0; i < chars; i++) {
        if (attrs[i].is_white) white_chars++;
        if (attrs[i].is_word_start) words++;
    }

    if (chars == 0) lines = 0;

	g_snprintf (tmpbuf, BUFFER_SIZE, "<b>%s</b>: %d\n<b>%s</b>: %d\n<b>%s</b>: %d\n<b>%s</b>: %d\n<b>%s</b>: %d\n",
                _("Words"), words,
                _("Lines"), lines,
                _("Characters"), chars,
                _("White characters"), white_chars,
                _("Bytes"), bytes);

	g_free (text);
    g_free (attrs);

	return g_strdup(tmpbuf);
}

/*------------------------------------------------------------------------------*/

void
text_info_cb (GtkToolButton *toolbutton, gpointer data)
{
	GtkTreePath *path, *sort_path, *filter_path;
	GtkTreeIter n_iter;
	GtkTextBuffer *buffer;

	guint32 modified_date, created_date;
	gint modified_time, created_time;
	gchar *modified_date_str, *created_date_str, *date_str, *day_str;
	gchar *txtinfo_normal, *txtinfo_selection;
	gchar tmpbuf[BUFFER_SIZE];

	GUI *appGUI = (GUI *)data;

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

	gtk_tree_view_get_cursor (GTK_TREE_VIEW (appGUI->nte->notes_list), &sort_path, NULL);

    if (sort_path == NULL) return;
    filter_path = gtk_tree_model_sort_convert_path_to_child_path (GTK_TREE_MODEL_SORT(appGUI->nte->notes_sort), sort_path);
    gtk_tree_path_free (sort_path);

    if (filter_path == NULL) return;
    path = gtk_tree_model_filter_convert_path_to_child_path (GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter), filter_path);
    gtk_tree_path_free (filter_path);

    gtk_tree_model_get_iter (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &n_iter, path);
    gtk_tree_model_get (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &n_iter, 
                        N_COLUMN_LAST_CHANGES_DATE_JULIAN, &modified_date, 
                        N_COLUMN_LAST_CHANGES_TIME, &modified_time,
                        N_COLUMN_CREATE_DATE_JULIAN, &created_date,
                        N_COLUMN_CREATE_TIME, &created_time,
                        -1);

	date_str = utl_date_time_print_default (modified_date, modified_time, FALSE);
	day_str = utl_date_print_j (modified_date, DATE_DAY_OF_WEEK_NAME, config.override_locale_settings);
	modified_date_str = g_strdup_printf ("%s (%s)", date_str, day_str);
	g_free (date_str);
	g_free (day_str);

	date_str = utl_date_time_print_default (created_date, created_time, FALSE);
	day_str = utl_date_print_j (created_date, DATE_DAY_OF_WEEK_NAME, config.override_locale_settings);
	created_date_str = g_strdup_printf ("%s (%s)", date_str, day_str);
	g_free (date_str);
	g_free (day_str);

	txtinfo_normal = get_buffer_info_string (buffer, FALSE);
	txtinfo_selection = get_buffer_info_string (buffer, TRUE);

	if (strlen(txtinfo_selection)) {
		g_snprintf (tmpbuf, BUFFER_SIZE, "<u>%s:</u>\n\n%s\n<u>%s:</u>\n\n%s\n\n<b>%s</b>: %s\n<b>%s</b>: %s",
					_("Document"), txtinfo_normal, _("Selection"), txtinfo_selection,
					_("Created"), created_date_str,
					_("Modified"), modified_date_str);
	} else {
		g_snprintf (tmpbuf, BUFFER_SIZE, "<u>%s:</u>\n\n%s\n\n<b>%s</b>: %s\n<b>%s</b>: %s",
					_("Document"), txtinfo_normal,
					_("Created"), created_date_str,
					_("Modified"), modified_date_str);
	}

	g_free (txtinfo_selection);
	g_free (txtinfo_normal);

    g_free (modified_date_str);
    g_free (created_date_str);
    gtk_tree_path_free (path);

	utl_gui_create_dialog (GTK_MESSAGE_INFO, tmpbuf, GTK_WINDOW (appGUI->main_window));
}

/*------------------------------------------------------------------------------*/

gint
notes_list_dbclick_cb(GtkWidget * widget, GdkEventButton * event, gpointer func_data) {

    GUI *appGUI = (GUI *)func_data;

    if ((event->type==GDK_2BUTTON_PRESS) && (event->button == 1)) {
        if (gtk_tree_selection_count_selected_rows (appGUI->nte->notes_list_selection) == 1) {
            notes_enter_password (appGUI);
        }
        return TRUE;
    }

    return FALSE;
}

/*------------------------------------------------------------------------------*/

void
text_buffer_modified_cb (GtkTextBuffer *textbuffer, gpointer user_data) {

    GUI *appGUI = (GUI *)user_data;

    if (appGUI->nte->buffer_check_modify_enable == FALSE) {
        appGUI->nte->changed = FALSE;
        gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->save_button), FALSE);
    } else {
        appGUI->nte->changed = TRUE;
        gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->save_button), TRUE);
    }
}

/*------------------------------------------------------------------------------*/

void
notes_item_selected (GtkTreeSelection *selection, gpointer data) {
    gint selection_size;
    GUI *appGUI = (GUI *)data;

    selection_size = gtk_tree_selection_count_selected_rows (selection);

    gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->edit_button), selection_size == 1);
    gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->delete_button), selection_size > 0);
}

#ifdef HAVE_LIBWEBKIT
/*------------------------------------------------------------------------------*/
static void
text_found (WebKitFindController *find_controller,
               guint                 match_count,
               gpointer              user_data) {
    GUI *appGUI = (GUI *)user_data;
    utl_gui_change_bg_widget_state (appGUI->nte->find_entry, COLOR_BG_OK, appGUI);
}

static void
text_not_found (WebKitFindController *find_controller,
               gpointer              user_data) {
    GUI *appGUI = (GUI *)user_data;
    utl_gui_change_bg_widget_state (appGUI->nte->find_entry, COLOR_BG_FAIL, appGUI);
}
#endif  /* HAVE_LIBWEBKIT */
/*------------------------------------------------------------------------------*/

void
find (gchar *find_text, GtkTextIter *iter, gboolean forward, GUI *appGUI) {

GtkTextBuffer *buffer;
GtkTextIter match_start, match_end;
GtkTextMark *found_pos;
gboolean flags, result;

    if (strlen(find_text)) {

#ifdef HAVE_LIBWEBKIT

		if (iter == NULL) { /* webkit */
			WebKitFindController *find_controller;
			WebKitFindOptions case_sensitive, direction;
			flags = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(appGUI->nte->find_case_checkbutton));
			case_sensitive = flags ? WEBKIT_FIND_OPTIONS_NONE : WEBKIT_FIND_OPTIONS_CASE_INSENSITIVE;
			direction = forward ? WEBKIT_FIND_OPTIONS_NONE : WEBKIT_FIND_OPTIONS_BACKWARDS;
			find_controller = webkit_web_view_get_find_controller(appGUI->nte->html_webkitview);
			webkit_find_controller_search(find_controller, find_text, case_sensitive|direction, G_MAXUINT);
		} else {

#endif  /* HAVE_LIBWEBKIT */

			flags = GTK_SOURCE_SEARCH_TEXT_ONLY;

			if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(appGUI->nte->find_case_checkbutton)) == FALSE) {
				flags |= GTK_SOURCE_SEARCH_CASE_INSENSITIVE;
			}

			buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

			if (forward == TRUE) {
				result = gtk_source_iter_forward_search (iter, find_text, flags, &match_start, &match_end, NULL);
			} else {
				result = gtk_source_iter_backward_search (iter, find_text, flags, &match_start, &match_end, NULL);
			}

			if (result) {

				utl_gui_change_bg_widget_state (appGUI->nte->find_entry, COLOR_BG_OK, appGUI);
				gtk_text_buffer_select_range (buffer, &match_start, &match_end);
				if (forward == TRUE) {
					gtk_text_buffer_create_mark (buffer, "last_pos", &match_end, FALSE);
				} else {
					gtk_text_buffer_create_mark (buffer, "last_pos", &match_start, FALSE);
				}

				found_pos = gtk_text_buffer_create_mark (buffer, "found_pos", &match_end, FALSE);
				gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (appGUI->nte->editor_textview), found_pos); 
				gtk_text_buffer_delete_mark (buffer, found_pos);

				appGUI->nte->find_next_flag = TRUE;

			} else {
				utl_gui_change_bg_widget_state (appGUI->nte->find_entry, COLOR_BG_FAIL, appGUI);
				if (gtk_text_buffer_get_mark (buffer, "last_pos") != NULL) {
					gtk_text_buffer_delete_mark_by_name (buffer, "last_pos");
				}
				appGUI->nte->find_next_flag = FALSE;
			}
		}
#ifdef HAVE_LIBWEBKIT
	}
#endif  /* HAVE_LIBWEBKIT */

}

/*------------------------------------------------------------------------------*/

void
find_entry_action (gboolean forward, GUI *appGUI) {

gchar *find_text;
gboolean wkflag;
GtkTextBuffer *buffer;
GtkTextMark *last_pos;
GtkTextIter iter;

	find_text = g_strdup(gtk_entry_get_text(GTK_ENTRY(appGUI->nte->find_entry)));

	wkflag = FALSE;
#ifdef HAVE_LIBWEBKIT
	wkflag = TRUE;
#endif  /* HAVE_LIBWEBKIT */

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (appGUI->nte->readonly_checkbutton)) && wkflag == TRUE) {

		find (find_text, NULL, forward, appGUI);

	} else {

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));

		if (appGUI->nte->find_next_flag == TRUE) {

			last_pos = gtk_text_buffer_get_mark (buffer, "last_pos");

			if (last_pos == NULL) {
				appGUI->nte->find_next_flag = FALSE;
				g_free(find_text);
				return;
			}

			gtk_text_buffer_get_iter_at_mark (buffer, &iter, last_pos);
			find (find_text, &iter, forward, appGUI);

		} else {
			if (forward == TRUE) {
				gtk_text_buffer_get_start_iter (buffer, &iter);
			} else {
				gtk_text_buffer_get_end_iter (buffer, &iter);
			}
			find (find_text, &iter, forward, appGUI);
		}

		g_free(find_text);
	}
}

/*------------------------------------------------------------------------------*/

gint
find_entry_key_press_cb (GtkWidget *widget, GdkEventKey *event, gpointer data) {

    GUI *appGUI = (GUI *)data;

    if (event->keyval == GDK_KEY_Escape) {
            editor_find_text_hide_cb (widget, appGUI);
            appGUI->nte->find_next_flag = FALSE;
            return TRUE;
    } else if (event->keyval == GDK_KEY_Return) {
            find_entry_action (TRUE, appGUI);
            return TRUE;
    }
    return FALSE;
}

/*------------------------------------------------------------------------------*/

void
case_sensitive_toggle_cb (GtkToggleButton *togglebutton, gpointer data) {

    GUI *appGUI = (GUI *)data;

    appGUI->nte->find_next_flag = FALSE;
}

/*------------------------------------------------------------------------------*/

gboolean
notes_category_combo_box_focus_cb (GtkWidget *widget, GtkDirectionType *arg1, gpointer user_data) {
    return TRUE;
}

/*------------------------------------------------------------------------------*/

void
update_notes_items (GUI *appGUI) {
gint i;
gchar tmpbuf[BUFFER_SIZE];

    i = gtk_tree_model_iter_n_children (GTK_TREE_MODEL(appGUI->nte->notes_filter), NULL);

    if (!i) {
        g_snprintf(tmpbuf, BUFFER_SIZE, "<i>%s</i>", _("no entries"));
    } else {
        g_snprintf(tmpbuf, BUFFER_SIZE, "<i>%4d %s</i>", i, ngettext ("entry", "entries", i));
    }

    gtk_label_set_markup (GTK_LABEL (appGUI->nte->n_items_label), tmpbuf);

}

/*------------------------------------------------------------------------------*/

void
refresh_notes (GUI *appGUI) {

GtkTreeIter iter;
gboolean has_next;
guint32 last_changes_date, create_date;
gint last_changes_time, create_time;
gchar *last_changes_time_str, *create_time_str;

    has_next = gtk_tree_model_get_iter_first (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter);
    while (has_next) {

        gtk_tree_model_get(GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter,
                N_COLUMN_LAST_CHANGES_DATE_JULIAN, &last_changes_date,
                N_COLUMN_LAST_CHANGES_TIME, &last_changes_time,
                N_COLUMN_CREATE_DATE_JULIAN, &create_date,
                N_COLUMN_CREATE_TIME, &create_time, -1);

        last_changes_time_str = g_strdup(get_date_time_str(last_changes_date, last_changes_time));
        create_time_str = g_strdup(get_date_time_str(create_date, create_time));
        gtk_list_store_set(appGUI->nte->notes_list_store, &iter,
                N_COLUMN_LAST_CHANGES_DATE, last_changes_time_str,
                N_COLUMN_CREATE_DATE, create_time_str, -1);
        g_free(last_changes_time_str);
        g_free(create_time_str);
        has_next = gtk_tree_model_iter_next (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter);
    }

    update_notes_items(appGUI);
}

/*------------------------------------------------------------------------------*/

void
notes_category_filter_cb (GtkComboBox *widget, gpointer user_data) {

    GUI *appGUI = (GUI *)user_data;

    appGUI->nte->filter_index = gtk_combo_box_get_active (GTK_COMBO_BOX (widget));

    if (appGUI->nte->filter_index != -1) {
        gtk_tree_model_filter_refilter (GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter));
        update_notes_items (appGUI);
    }
}

/*------------------------------------------------------------------------------*/

gboolean
notes_list_filter_cb (GtkTreeModel *model, GtkTreeIter *iter, gpointer data) {

gchar *category;
gchar *value;
gboolean result;

    GUI *appGUI = (GUI *)data;

    gtk_tree_model_get(model, iter, N_COLUMN_NAME, &value, N_COLUMN_CATEGORY, &category, -1);

    if (appGUI->nte->filter_index && (utl_gui_list_store_get_text_index(appGUI->opt->notes_category_store, category) != appGUI->nte->filter_index)) {
        result = FALSE;
    } else if (value == NULL) {
        result = FALSE;
    } else {
        const gchar *text = gtk_entry_get_text(GTK_ENTRY(appGUI->nte->notes_find_entry));

        if (text == NULL) {
            result = TRUE;
        } else
            if (!g_utf8_strlen(text, -1)) {
            result = TRUE;
        } else {
            result = utl_text_strcasestr(value, text);
        }
    }

    g_free(category);
    g_free(value);
    return result;
}

/*------------------------------------------------------------------------------*/

gint
notes_column_sort_function (GtkTreeModel *model, GtkTreeIter *iter_a, GtkTreeIter *iter_b, gpointer user_data) {
gboolean type_a, type_b;
gchar *name_a, *name_b;
gchar *category_a, *category_b;
guint32 last_changes_date_a, last_changes_date_b, create_date_a, create_date_b;
gint last_changes_time_a, last_changes_time_b, create_time_a, create_time_b;
gint diff;
gint sort_column = GPOINTER_TO_INT(user_data);

    if(iter_a == NULL || iter_b == NULL) {
        return 0;
    }

    switch (sort_column) {
        case N_COLUMN_TYPE:
            gtk_tree_model_get(model, iter_a, N_COLUMN_ENCRYPTED, &type_a, -1);
            gtk_tree_model_get(model, iter_b, N_COLUMN_ENCRYPTED, &type_b, -1);
            if (type_a == type_b) {
                diff = 0;
            } else {
                diff = type_a == TRUE ? 1 : -1;
            }
            break;
        case N_COLUMN_NAME:
            gtk_tree_model_get(model, iter_a, N_COLUMN_NAME, &name_a, -1);
            gtk_tree_model_get(model, iter_b, N_COLUMN_NAME, &name_b, -1);
            diff = utl_text_strcmp(name_a, name_b);
            g_free(name_a);
            g_free(name_b);
            break;
        case N_COLUMN_CATEGORY:
            gtk_tree_model_get(model, iter_a, N_COLUMN_CATEGORY, &category_a, -1);
            gtk_tree_model_get(model, iter_b, N_COLUMN_CATEGORY, &category_b, -1);
            diff = utl_text_strcmp(category_a, category_b);
            g_free(category_a);
            g_free(category_b);
            break;
        case N_COLUMN_LAST_CHANGES_DATE:
            gtk_tree_model_get(model, iter_a,
							   N_COLUMN_LAST_CHANGES_DATE_JULIAN, &last_changes_date_a,
							   N_COLUMN_LAST_CHANGES_TIME, &last_changes_time_a, -1);
            gtk_tree_model_get(model, iter_b,
							   N_COLUMN_LAST_CHANGES_DATE_JULIAN, &last_changes_date_b,
							   N_COLUMN_LAST_CHANGES_TIME, &last_changes_time_b, -1);
            diff = utl_date_time_compare_js(last_changes_date_a, last_changes_time_a,
											last_changes_date_b, last_changes_time_b);
            break;
        case N_COLUMN_CREATE_DATE:
            gtk_tree_model_get(model, iter_a,
                    N_COLUMN_CREATE_DATE_JULIAN, &create_date_a,
                    N_COLUMN_CREATE_TIME, &create_time_a, -1);
            gtk_tree_model_get(model, iter_b,
                    N_COLUMN_CREATE_DATE_JULIAN, &create_date_b,
                    N_COLUMN_CREATE_TIME, &create_time_b, -1);
            diff = utl_date_time_compare_js(create_date_a, create_time_a,
											create_date_b, create_time_b);
            break;
        default:
            diff = 0;
    }

    return diff;
}

/*------------------------------------------------------------------------------*/
void notes_sort_column_changed_cb (GtkTreeSortable *sortable, gpointer user_data)
{
    GtkSortType sort_order;

    gtk_tree_sortable_get_sort_column_id(sortable, &config.notes_sorting_column, &sort_order);
    config.notes_sorting_order = sort_order;
}

/*------------------------------------------------------------------------------*/

#ifdef HAVE_GSPELL
static void
notes_spell_check_cb(GtkToggleToolButton *toggle_tool_button, gpointer data) {
    GUI *appGUI = (GUI *) data;
    gboolean enable_spellcheck = gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(toggle_tool_button));

    utl_gui_set_enable_spell_check(appGUI->nte->editor_spelltextview, enable_spellcheck);
}
#endif /* HAVE_GSPELL */

/*------------------------------------------------------------------------------*/

void
store_note_columns_info (GUI *appGUI) {

	gint n;

	config.notes_column_idx_0 = utl_gui_get_column_position (appGUI->nte->notes_columns[N_COLUMN_TYPE], 
															 GTK_TREE_VIEW(appGUI->nte->notes_list), MAX_VISIBLE_NOTE_COLUMNS, appGUI);
	config.notes_column_idx_1 = utl_gui_get_column_position (appGUI->nte->notes_columns[N_COLUMN_NAME], 
															 GTK_TREE_VIEW(appGUI->nte->notes_list), MAX_VISIBLE_NOTE_COLUMNS, appGUI);
	config.notes_column_idx_2 = utl_gui_get_column_position (appGUI->nte->notes_columns[N_COLUMN_CATEGORY], 
															 GTK_TREE_VIEW(appGUI->nte->notes_list), MAX_VISIBLE_NOTE_COLUMNS, appGUI);
	config.notes_column_idx_3 = utl_gui_get_column_position (appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE], 
															 GTK_TREE_VIEW(appGUI->nte->notes_list), MAX_VISIBLE_NOTE_COLUMNS, appGUI);
	config.notes_column_idx_4 = utl_gui_get_column_position (appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE], 
															 GTK_TREE_VIEW(appGUI->nte->notes_list), MAX_VISIBLE_NOTE_COLUMNS, appGUI);

	n = gtk_tree_view_column_get_width (gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 0));
	if (n > 1) {
		config.notes_column_idx_0_width = n;
	}
	n = gtk_tree_view_column_get_width (gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 1));
	if (n > 1) {
		config.notes_column_idx_1_width = n;
	}
	n = gtk_tree_view_column_get_width (gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 2));
	if (n > 1) {
		config.notes_column_idx_2_width = n;
	}
	n = gtk_tree_view_column_get_width (gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 3));
	if (n > 1) {
		config.notes_column_idx_3_width = n;
	}
	n = gtk_tree_view_column_get_width (gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 4));
	if (n > 1) {
		config.notes_column_idx_4_width = n;
	}
}

/*------------------------------------------------------------------------------*/

void
set_note_columns_width (GUI *appGUI) {

GtkTreeViewColumn   *col;
gint w;

	w = 2 * utl_gui_get_sw_vscrollbar_width (appGUI->nte->scrolled_win);

	col = gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 0);
	if (gtk_tree_view_column_get_visible(col) == TRUE && config.notes_column_idx_0_width > 0) {
		gtk_tree_view_column_set_fixed_width (col, config.notes_column_idx_0_width);
	}
	col = gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 1);
	if (gtk_tree_view_column_get_visible(col) == TRUE && config.notes_column_idx_1_width > 0) {
		gtk_tree_view_column_set_fixed_width (col, config.notes_column_idx_1_width);
	}
	col = gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 2);
	if (gtk_tree_view_column_get_visible(col) == TRUE && config.notes_column_idx_2_width > 0) {
		gtk_tree_view_column_set_fixed_width (col, config.notes_column_idx_2_width);
	}
	col = gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 3);
	if (gtk_tree_view_column_get_visible(col) == TRUE && config.notes_column_idx_3_width > 0) {
		gtk_tree_view_column_set_fixed_width (col, config.notes_column_idx_3_width);
	}
	col = gtk_tree_view_get_column (GTK_TREE_VIEW(appGUI->nte->notes_list), 4);
	if (gtk_tree_view_column_get_visible(col) == TRUE && config.notes_column_idx_4_width > w) {
		gtk_tree_view_column_set_fixed_width (col, config.notes_column_idx_4_width - w);
	}
}

/*------------------------------------------------------------------------------*/

void
editor_cursor_move_cb (GtkTextBuffer *buffer, GtkTextIter *new_location, 
					   GtkTextMark *mark, gpointer user_data)
{
GtkTextIter iter;
gint row, col, nrow, ncol;
gchar tmpbuf[BUFFER_SIZE];

	GUI *appGUI = (GUI *)user_data;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (appGUI->nte->readonly_checkbutton))) {
		return;
	}

	gtk_text_buffer_get_iter_at_mark(buffer, &iter, gtk_text_buffer_get_insert(buffer));

	row = gtk_text_iter_get_line (&iter) + 1;
	nrow = gtk_text_buffer_get_line_count (buffer);
	g_snprintf(tmpbuf, BUFFER_SIZE, "<tt>%d/%d</tt>", row, nrow);
	gtk_label_set_markup (GTK_LABEL (appGUI->nte->nrow_label), tmpbuf);

	col = gtk_text_iter_get_line_offset (&iter) + 1;
	ncol = gtk_text_iter_get_chars_in_line (&iter);
	if (ncol < col) ncol = col;
	g_snprintf(tmpbuf, BUFFER_SIZE, "<tt>%d/%d</tt>", col, ncol);
	gtk_label_set_markup (GTK_LABEL (appGUI->nte->ncol_label), tmpbuf);

}

/*------------------------------------------------------------------------------*/

void
update_readonly_view (GUI *appGUI) {

gchar *output;
gchar *text;
GtkTextBuffer *buffer;

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));
	text = utl_gui_text_buffer_get_text_with_tags (buffer);

	output = utl_text_to_html_page (text, pango_font_description_get_family (appGUI->nte->fd_notes_font), 
									NULL, NULL, NULL, NULL, NULL);
	g_free(text);

#ifdef HAVE_LIBWEBKIT
	webkit_settings_set_default_font_size (webkit_web_view_get_settings(appGUI->nte->html_webkitview), PANGO_PIXELS(pango_font_description_get_size (appGUI->nte->fd_notes_font)));
	webkit_web_view_load_html(appGUI->nte->html_webkitview, output, "file://");
#endif  /* HAVE_LIBWEBKIT */

	g_free(output);
}

/*------------------------------------------------------------------------------*/

void
readonly_toggle_cb (GtkToggleButton *togglebutton, gpointer data) {

    GUI *appGUI = (GUI *)data;
	GtkAdjustment *vadj = NULL;
	gint nbtns, i;
	gboolean s = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (appGUI->nte->readonly_checkbutton));

	gtk_text_view_set_editable (GTK_TEXT_VIEW (appGUI->nte->editor_textview), !s);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (appGUI->nte->editor_textview), !s);
	gtk_widget_set_sensitive (appGUI->nte->font_picker, !s);

	if (s == TRUE) {    /* read-only */

		update_readonly_view (appGUI);

		gtk_label_set_text(GTK_LABEL(appGUI->nte->nrow_label), "-");
		gtk_label_set_text(GTK_LABEL(appGUI->nte->ncol_label), "-");
#ifdef HAVE_LIBWEBKIT
		gtk_widget_hide(appGUI->nte->editor_viewport);
		gtk_widget_show(appGUI->nte->ro_editor_viewport);
#endif /* HAVE_LIBWEBKIT */

		while (gtk_events_pending ()) gtk_main_iteration ();    /* FIXME: update drawing of scrolled window */
                                                                /* probably use 'size-allocate' signal later */

		vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW(appGUI->nte->ro_editor_scrolledwindow));
		gtk_adjustment_set_value(GTK_ADJUSTMENT(vadj), appGUI->nte->htmlViewPosition);
		gtk_scrolled_window_set_vadjustment (GTK_SCROLLED_WINDOW(appGUI->nte->ro_editor_scrolledwindow), GTK_ADJUSTMENT(vadj));
	
	} else {
		
		vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW(appGUI->nte->ro_editor_scrolledwindow));
		appGUI->nte->htmlViewPosition = gtk_adjustment_get_value(GTK_ADJUSTMENT(vadj));

#ifdef HAVE_LIBWEBKIT
		gtk_widget_show(appGUI->nte->editor_viewport);
		gtk_widget_hide(appGUI->nte->ro_editor_viewport);
#endif /* HAVE_LIBWEBKIT */

		editor_cursor_move_cb (gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview)),
							   NULL, NULL, appGUI);
		gtk_widget_grab_focus (appGUI->nte->editor_textview);
	}

	appGUI->nte->note_read_only = s;

	for (i = 0, nbtns = gtk_toolbar_get_n_items(appGUI->nte->editor_toolbar) - 1; i < nbtns; i++) {
		GtkToolItem *item = gtk_toolbar_get_nth_item(appGUI->nte->editor_toolbar, i);
		if (GTK_IS_TOOL_BUTTON(item) == TRUE) {
			gtk_widget_set_sensitive(GTK_WIDGET(item), !s);
		}
	}

}

/*------------------------------------------------------------------------------*/

gboolean
notes_search_entry_changed_cb (GtkWidget *widget, GdkEventKey *event, gpointer user_data) {

GtkTreePath *path;
GtkTreeIter iter;

    GUI *appGUI = (GUI *)user_data;

    gtk_tree_model_filter_refilter(GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter));

    if(strlen(gtk_entry_get_text (GTK_ENTRY(appGUI->nte->notes_find_entry)))) {
		gboolean has_next = gtk_tree_model_get_iter_first (GTK_TREE_MODEL(appGUI->nte->notes_filter), &iter);
		if (has_next) {
			path = gtk_tree_path_new_first();
			if (path != NULL) {
				gtk_tree_view_set_cursor (GTK_TREE_VIEW (appGUI->nte->notes_list), path, NULL, FALSE);
				gtk_tree_path_free (path);
			}
		} 
    }
		
	update_notes_items (appGUI);

    return FALSE;
}

/*------------------------------------------------------------------------------*/

void
nte_clear_find_cb (GtkWidget *widget, gpointer user_data)
{
	GUI *appGUI = (GUI *) user_data;
    if (strlen(gtk_entry_get_text(GTK_ENTRY(appGUI->nte->notes_find_entry)))) {
        gtk_entry_set_text(GTK_ENTRY(appGUI->nte->notes_find_entry), "");
		notes_search_entry_changed_cb (NULL, NULL, appGUI);
	}
	gtk_widget_grab_focus (appGUI->nte->notes_find_entry);
}

/*------------------------------------------------------------------------------*/

void
nte_font_selected (GtkFontButton *widget, gpointer user_data) {

	GUI *appGUI = (GUI *) user_data;

    pango_font_description_free(appGUI->nte->fd_notes_font);
    appGUI->nte->fd_notes_font = pango_font_description_from_string(gtk_font_button_get_font_name (GTK_FONT_BUTTON(widget)));	
    gtk_widget_override_font (GTK_WIDGET(appGUI->nte->editor_textview), appGUI->nte->fd_notes_font);

}

/*------------------------------------------------------------------------------*/

void
editor_find_backward_cb (GtkWidget *widget, gpointer user_data) {

	GUI *appGUI = (GUI *)user_data;
	find_entry_action (FALSE, appGUI);
}

void
editor_find_forward_cb (GtkWidget *widget, gpointer user_data) {

	GUI *appGUI = (GUI *)user_data;
	find_entry_action (TRUE, appGUI);
}

/*------------------------------------------------------------------------------*/

#if defined(BACKUP_SUPPORT) && defined(HAVE_LIBGRINGOTTS)

static void
button_create_backup_cb (GtkToolButton *toolbutton, gpointer data)
{
    GUI *appGUI = (GUI *)data;
    backup_create (appGUI);
}

static void
button_restore_backup_cb (GtkToolButton *toolbutton, gpointer data)
{
    GUI *appGUI = (GUI *)data;
    backup_restore (appGUI);
}

#endif  /* BACKUP_SUPPORT && HAVE_LIBGRINGOTTS */

/*------------------------------------------------------------------------------*/
static void
fill_notes_toolbar(GtkToolbar *toolbar, GUI *appGUI) {
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_NOTES_ADD, _("New note"), notes_add_item_cb), -1);
    appGUI->nte->edit_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_NOTES_EDIT, _("Edit note"), notes_edit_item_cb);
    gtk_toolbar_insert(toolbar, appGUI->nte->edit_button, -1);
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
    appGUI->nte->delete_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_NOTES_REMOVE, _("Delete note"), notes_delete_item_cb);
    gtk_toolbar_insert(toolbar, appGUI->nte->delete_button, -1);
    gui_append_toolbar_spring(toolbar);
#if defined(BACKUP_SUPPORT) && defined(HAVE_LIBGRINGOTTS)
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_BACKUP, _("Backup data"), button_create_backup_cb), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_RESTORE, _("Restore data"), button_restore_backup_cb), -1);
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
#endif  /* BACKUP_SUPPORT && HAVE_LIBGRINGOTTS */
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_PREFERENCES, _("Preferences"), show_preferences_window_cb), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_ABOUT, _("About"), gui_show_about_window_cb), -1);
    appGUI->nte->quit_button = gui_create_toolbar_button(appGUI, "application-exit", _("Quit"), gui_quit_osmo_cb);
    gtk_toolbar_insert(toolbar, appGUI->nte->quit_button, -1);

    gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->edit_button), FALSE);
    gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->delete_button), FALSE);
}
/*------------------------------------------------------------------------------*/
static void
fill_editor_toolbar(GtkToolbar *toolbar, GtkTextBuffer *buffer, GUI *appGUI) {
    appGUI->nte->save_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_SAVE, _("Save note"), editor_save_buffer_cb);
    gtk_toolbar_insert(toolbar, appGUI->nte->save_button, -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_FIND, _("Find"), editor_find_text_show_cb), -1);
#ifdef HAVE_GSPELL
    appGUI->nte->spell_check_button = gui_create_toolbar_toggle_button(appGUI, OSMO_STOCK_EDITOR_SPELL_CHECKER, _("Toggle spell checker"), notes_spell_check_cb);
    gtk_toolbar_insert(toolbar, appGUI->nte->spell_check_button, -1);
#endif /* HAVE_GSPELL */
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
    appGUI->nte->bold_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_BOLD, _("Bold"), set_text_attribute_cb);
    gtk_text_buffer_create_tag (buffer, "bold", "weight", PANGO_WEIGHT_BOLD, NULL);
    g_object_set_data (G_OBJECT (appGUI->nte->bold_button), "tag", "bold");
    gtk_toolbar_insert(toolbar, appGUI->nte->bold_button, -1);
    appGUI->nte->italic_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_ITALIC, _("Italic"), set_text_attribute_cb);
    gtk_text_buffer_create_tag (buffer, "italic", "style", PANGO_STYLE_ITALIC, NULL);
    g_object_set_data (G_OBJECT (appGUI->nte->italic_button), "tag", "italic");
    gtk_toolbar_insert(toolbar, appGUI->nte->italic_button, -1);
    appGUI->nte->underline_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_UNDERLINE, _("Underline"), set_text_attribute_cb);
    gtk_text_buffer_create_tag (buffer, "underline", "underline", PANGO_UNDERLINE_SINGLE, NULL);
    g_object_set_data (G_OBJECT (appGUI->nte->underline_button), "tag", "underline");
    gtk_toolbar_insert(toolbar, appGUI->nte->underline_button, -1);
    appGUI->nte->strikethrough_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_STRIKETHROUGH, _("Strikethrough"), set_text_attribute_cb);
    gtk_text_buffer_create_tag (buffer, "strike", "strikethrough", TRUE, NULL);
    g_object_set_data (G_OBJECT (appGUI->nte->strikethrough_button), "tag", "strike");
    gtk_toolbar_insert(toolbar, appGUI->nte->strikethrough_button, -1);
    appGUI->nte->highlight_button = gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_HIGHLIGHT, _("Highlight"), set_text_attribute_cb);
    gtk_text_buffer_create_tag (buffer, "mark_color", "background", "#FFFF00", NULL);
    g_object_set_data (G_OBJECT (appGUI->nte->highlight_button), "tag", "mark_color");
    gtk_toolbar_insert(toolbar, appGUI->nte->highlight_button, -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_CLEAR, _("Clear attributes"), clear_text_attributes_cb), -1);
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_OPEN_URL, _("Open URL links"), open_url_links_cb), -1);
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_INSERT_DATE_TIME, _("Insert current date and time"), insert_current_date_and_time_cb), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_INSERT_SEPARATOR, _("Insert separator"), insert_separator_cb), -1);
    gtk_toolbar_insert(toolbar, gtk_separator_tool_item_new(), -1);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_EDITOR_INFO, _("Statistics"), text_info_cb), -1);
    gui_append_toolbar_spring(toolbar);
    gtk_toolbar_insert(toolbar, gui_create_toolbar_button(appGUI, OSMO_STOCK_CLOSE, _("Close editor"), editor_close_cb), -1);
}
/*------------------------------------------------------------------------------*/

void 
gui_create_notes (GUI *appGUI) {

GtkWidget           *vbox1;
GtkWidget           *vbox2;
GtkWidget           *vbox3;
GtkWidget           *hbox1;
GtkTextBuffer       *buffer;
GtkToolbar          *notes_toolbar;
GtkCellRenderer     *renderer;
GtkWidget           *hseparator;
GtkWidget           *label;
GtkWidget           *close_button;
GtkWidget           *find_backward_button;
GtkWidget           *find_forward_button;
GtkWidget           *table;
gint i, n;
#ifdef HAVE_LIBWEBKIT
WebKitFindController *find_controller;
#endif /* HAVE_LIBWEBKIT */

gchar tmpbuf[BUFFER_SIZE];

gint columns_order[MAX_VISIBLE_NOTE_COLUMNS];

gint nt_columns[MAX_VISIBLE_NOTE_COLUMNS] = { 
		N_COLUMN_TYPE, N_COLUMN_NAME, N_COLUMN_CATEGORY, N_COLUMN_LAST_CHANGES_DATE, N_COLUMN_CREATE_DATE
};


    vbox1 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
    gtk_widget_show (vbox1);
    g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s</b>", _("Notes"));
    gui_add_to_notebook (vbox1, tmpbuf, appGUI);

    appGUI->nte->vbox = GTK_BOX(vbox1);

    if (config.hide_notes == TRUE) {
        gtk_widget_hide(GTK_WIDGET(appGUI->nte->vbox));
    }

    appGUI->nte->vbox_selector = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
    gtk_widget_show (appGUI->nte->vbox_selector);
    gtk_box_pack_start (GTK_BOX(appGUI->nte->vbox), appGUI->nte->vbox_selector, TRUE, TRUE, 0);
    appGUI->nte->vbox_editor = gtk_box_new (GTK_ORIENTATION_VERTICAL, 1);
    gtk_widget_show (appGUI->nte->vbox_editor);
    gtk_box_pack_start (GTK_BOX(appGUI->nte->vbox), appGUI->nte->vbox_editor, TRUE, TRUE, 0);

    /*-------------------------------------------------------------------------------------*/

    notes_toolbar = GTK_TOOLBAR(gtk_toolbar_new());
    gtk_box_pack_start(GTK_BOX(appGUI->nte->vbox_selector), GTK_WIDGET(notes_toolbar), FALSE, FALSE, 0);
    gtk_toolbar_set_style (notes_toolbar, GTK_TOOLBAR_ICONS);
    gtk_toolbar_set_icon_size(notes_toolbar, GTK_ICON_SIZE_LARGE_TOOLBAR);

    fill_notes_toolbar(notes_toolbar, appGUI);

    gtk_widget_show_all(GTK_WIDGET(notes_toolbar));

    /*-------------------------------------------------------------------------------------*/
    appGUI->nte->editor_toolbar = GTK_TOOLBAR(gtk_toolbar_new());
    gtk_box_pack_start(GTK_BOX(appGUI->nte->vbox_editor), GTK_WIDGET(appGUI->nte->editor_toolbar), FALSE, FALSE, 0);

    gtk_toolbar_set_style (appGUI->nte->editor_toolbar, GTK_TOOLBAR_ICONS);
    gtk_toolbar_set_icon_size(appGUI->nte->editor_toolbar, GTK_ICON_SIZE_LARGE_TOOLBAR);

    /*-------------------------------------------------------------------------------------*/
    /* selector */

	vbox2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_show (vbox2);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->vbox_selector), vbox2, TRUE, TRUE, 0);
	gtk_widget_set_margin_left(GTK_WIDGET(vbox2), 8);
	gtk_widget_set_margin_right(GTK_WIDGET(vbox2), 8);
	gtk_widget_set_margin_bottom(GTK_WIDGET(vbox2), 8);

    hseparator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (hseparator);
    gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 6);
    
    table = gtk_grid_new ();
    gtk_widget_show (table);
    gtk_box_pack_start (GTK_BOX (vbox2), table, FALSE, TRUE, 0);
    gtk_grid_set_column_spacing (GTK_GRID (table), 4);

    g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s:</b>", _("Category"));
    label = gtk_label_new (tmpbuf);
    gtk_widget_show (label);
    gtk_label_set_use_markup (GTK_LABEL (label), TRUE);

    appGUI->nte->cf_combobox = gtk_combo_box_text_new ();
    gtk_widget_show (appGUI->nte->cf_combobox);
    gtk_combo_box_set_focus_on_click (GTK_COMBO_BOX (appGUI->nte->cf_combobox), FALSE);
    gtk_widget_set_can_focus(appGUI->nte->cf_combobox, FALSE);
    g_signal_connect(appGUI->nte->cf_combobox, "changed", 
                     G_CALLBACK(notes_category_filter_cb), appGUI);
    g_signal_connect(G_OBJECT(appGUI->nte->cf_combobox), "focus", 
                     G_CALLBACK(notes_category_combo_box_focus_cb), NULL);

    appGUI->nte->n_items_label = gtk_label_new ("");
    gtk_widget_show (appGUI->nte->n_items_label);
    gtk_label_set_use_markup (GTK_LABEL (appGUI->nte->n_items_label), TRUE);

	if (!config.gui_layout) {   /* vertical */

		gtk_grid_attach (GTK_GRID (table), label, 0, 0, 1, 1);

        gtk_widget_set_hexpand(appGUI->nte->cf_combobox, TRUE);
        gtk_grid_attach (GTK_GRID (table), appGUI->nte->cf_combobox, 1, 0, 1, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->n_items_label, 3, 0, 1, 1);
	} else {

		gtk_grid_attach (GTK_GRID (table), label, 3, 0, 1, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->cf_combobox, 4, 0, 1, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->n_items_label, 5, 0, 1, 1);
	}

	g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s:</b>", _("Search"));
	label = gtk_label_new (tmpbuf);
	gtk_widget_show (label);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
    gtk_widget_set_valign(label, GTK_ALIGN_CENTER);
    gtk_widget_set_halign(label, GTK_ALIGN_START);

	appGUI->nte->notes_find_entry = gtk_entry_new();
	gtk_entry_set_max_length(GTK_ENTRY(appGUI->nte->notes_find_entry), 128);
	gtk_widget_show (appGUI->nte->notes_find_entry);
	g_signal_connect (G_OBJECT(appGUI->nte->notes_find_entry), "key_release_event",
					  G_CALLBACK(notes_search_entry_changed_cb), appGUI);

	appGUI->nte->notes_find_clear_button = gtk_button_new_from_icon_name ("edit-clear", GTK_ICON_SIZE_BUTTON);
	gtk_widget_show (appGUI->nte->notes_find_clear_button);
	gtk_widget_set_can_focus (appGUI->nte->notes_find_clear_button, FALSE);
	gtk_button_set_relief (GTK_BUTTON(appGUI->nte->notes_find_clear_button), GTK_RELIEF_NONE);
	if (config.enable_tooltips) {
		gtk_widget_set_tooltip_text (appGUI->nte->notes_find_clear_button, _("Clear"));
	}
	g_signal_connect (G_OBJECT (appGUI->nte->notes_find_clear_button), "clicked",
						G_CALLBACK (nte_clear_find_cb), appGUI);

	if (!config.gui_layout) {   /* vertical */

		gtk_grid_attach (GTK_GRID (table), label, 0, 1, 1, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->notes_find_entry, 1, 1, 4, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->notes_find_clear_button, 5, 1, 1, 1);

		gtk_grid_set_row_spacing (GTK_GRID (table), 4);

	} else {

		gtk_grid_attach (GTK_GRID (table), label, 0, 0, 1, 1);

        gtk_widget_set_hexpand(appGUI->nte->notes_find_entry, TRUE);
		gtk_grid_attach (GTK_GRID (table), appGUI->nte->notes_find_entry, 1, 0, 1, 1);

		gtk_grid_attach (GTK_GRID (table), appGUI->nte->notes_find_clear_button, 2, 0, 1, 1);
	}

    hseparator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (hseparator);
    gtk_box_pack_start (GTK_BOX (vbox2), hseparator, FALSE, TRUE, 6);

    /*-------------------------------------------------------------------------------------*/

    appGUI->nte->editor_viewport = gtk_viewport_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->editor_viewport);
    gtk_viewport_set_shadow_type (GTK_VIEWPORT (appGUI->nte->editor_viewport), GTK_SHADOW_IN);
    gtk_box_pack_start (GTK_BOX (vbox2), appGUI->nte->editor_viewport, TRUE, TRUE, 0);

    appGUI->nte->scrolled_win = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->scrolled_win);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (appGUI->nte->scrolled_win), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (appGUI->nte->editor_viewport), appGUI->nte->scrolled_win);

    appGUI->nte->notes_list_store = gtk_list_store_new(NOTES_NUM_COLUMNS,
                                                       GDK_TYPE_PIXBUF, G_TYPE_STRING, G_TYPE_STRING,
                                                       G_TYPE_STRING, G_TYPE_UINT, G_TYPE_UINT,
                                                       G_TYPE_STRING, G_TYPE_UINT, G_TYPE_UINT,
                                                       G_TYPE_BOOLEAN, G_TYPE_UINT, G_TYPE_BOOLEAN,
                                                       G_TYPE_STRING, G_TYPE_STRING, G_TYPE_BOOLEAN);

    appGUI->nte->notes_filter = gtk_tree_model_filter_new(GTK_TREE_MODEL(appGUI->nte->notes_list_store), NULL);
    gtk_tree_model_filter_set_visible_func (GTK_TREE_MODEL_FILTER(appGUI->nte->notes_filter), 
                                            (GtkTreeModelFilterVisibleFunc)notes_list_filter_cb, 
                                            appGUI, NULL);

    appGUI->nte->notes_sort = gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(appGUI->nte->notes_filter));

    appGUI->nte->notes_list = gtk_tree_view_new_with_model(GTK_TREE_MODEL(appGUI->nte->notes_sort));
    gtk_tree_view_set_enable_search (GTK_TREE_VIEW(appGUI->nte->notes_list), FALSE);
    gtk_widget_show (appGUI->nte->notes_list);
    gtk_widget_set_can_default (appGUI->nte->notes_list, TRUE);

    g_signal_connect(G_OBJECT(appGUI->nte->notes_list), "button_press_event",
                     G_CALLBACK(notes_list_dbclick_cb), appGUI);

    appGUI->nte->notes_list_selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (appGUI->nte->notes_list));
    gtk_tree_selection_set_mode (appGUI->nte->notes_list_selection, GTK_SELECTION_MULTIPLE);
    g_signal_connect(G_OBJECT(appGUI->nte->notes_list_selection), "changed",
                     G_CALLBACK(notes_item_selected), appGUI);

    /* create columns */

	renderer = gtk_cell_renderer_pixbuf_new();  /* icon */
    appGUI->nte->notes_columns[N_COLUMN_TYPE] = gtk_tree_view_column_new_with_attributes(_("Type"),
																						 renderer,
																						 "pixbuf", N_COLUMN_TYPE,
																						 NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_TYPE]);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_TYPE], config.nte_visible_type_column);
	gtk_tree_view_column_set_reorderable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_TYPE]), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_TYPE]), TRUE);
    gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_TYPE]), GTK_TREE_VIEW_COLUMN_FIXED);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(G_OBJECT(renderer), "ellipsize", PANGO_ELLIPSIZE_END, NULL);
    appGUI->nte->notes_columns[N_COLUMN_NAME] = gtk_tree_view_column_new_with_attributes(_("Note name"), 
																						 renderer,
																						 "text", N_COLUMN_NAME,
																						 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_NAME], TRUE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_NAME]);
	gtk_tree_view_column_set_reorderable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_NAME]), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_NAME]), TRUE);
    gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_NAME]), GTK_TREE_VIEW_COLUMN_FIXED);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_CATEGORY] = gtk_tree_view_column_new_with_attributes(_("Category"),
																							 renderer,
																							 "text", N_COLUMN_CATEGORY,
																							 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_CATEGORY], config.nte_visible_category_column);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_CATEGORY]);
	gtk_tree_view_column_set_reorderable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CATEGORY]), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CATEGORY]), TRUE);
    gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CATEGORY]), GTK_TREE_VIEW_COLUMN_FIXED);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE] = gtk_tree_view_column_new_with_attributes(_("Last changes"),
																									  renderer,
																									  "text", N_COLUMN_LAST_CHANGES_DATE,
																									  NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE], config.nte_visible_last_changes_column);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE]);
	gtk_tree_view_column_set_reorderable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE]), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE]), TRUE);
    gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE]), GTK_TREE_VIEW_COLUMN_FIXED);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE_JULIAN] = gtk_tree_view_column_new_with_attributes(NULL,
																											 renderer, 
																											 "text", N_COLUMN_LAST_CHANGES_DATE_JULIAN, 
																											 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE_JULIAN], FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_DATE_JULIAN]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_TIME] = gtk_tree_view_column_new_with_attributes(NULL,
																									  renderer,
																									  "text", N_COLUMN_LAST_CHANGES_TIME,
																									  NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_TIME], FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_LAST_CHANGES_TIME]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE] = gtk_tree_view_column_new_with_attributes(_("Created"),
																								renderer,
																								"text", N_COLUMN_CREATE_DATE,
																								NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE], config.nte_visible_created_column);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE]);
	gtk_tree_view_column_set_reorderable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE]), TRUE);
	gtk_tree_view_column_set_resizable (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE]), TRUE);
    gtk_tree_view_column_set_sizing (GTK_TREE_VIEW_COLUMN(appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE]), GTK_TREE_VIEW_COLUMN_FIXED);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE_JULIAN] = gtk_tree_view_column_new_with_attributes(NULL,
																									   renderer,
																									   "text", N_COLUMN_CREATE_DATE_JULIAN,
																									   NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE_JULIAN], FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_CREATE_DATE_JULIAN]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_CREATE_TIME] = gtk_tree_view_column_new_with_attributes(NULL,
																								renderer, 
																								"text", N_COLUMN_CREATE_TIME,
																								NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_CREATE_TIME], FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_CREATE_TIME]);

	renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_REMEMBER_EDITOR_LINE] = gtk_tree_view_column_new_with_attributes(NULL,
																										 renderer,
																										 "text", N_COLUMN_REMEMBER_EDITOR_LINE,
																										 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_REMEMBER_EDITOR_LINE], FALSE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_REMEMBER_EDITOR_LINE]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_EDITOR_LINE] = gtk_tree_view_column_new_with_attributes(NULL,
																								renderer, 
																								"text", N_COLUMN_EDITOR_LINE,
																								NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_EDITOR_LINE], FALSE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_EDITOR_LINE]);

	renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_EDITOR_READONLY] = gtk_tree_view_column_new_with_attributes(NULL,
																								    renderer, 
																								    "text", N_COLUMN_EDITOR_READONLY,
																								    NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_EDITOR_READONLY], FALSE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_EDITOR_READONLY]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_FILENAME] = gtk_tree_view_column_new_with_attributes(NULL,
																							 renderer,
																							 "text", N_COLUMN_FILENAME,
																							 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_FILENAME], FALSE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_FILENAME]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_FONTNAME] = gtk_tree_view_column_new_with_attributes(NULL,
																							 renderer,
																							 "text", N_COLUMN_FONTNAME,
																							 NULL);
    gtk_tree_view_column_set_visible (appGUI->nte->notes_columns[N_COLUMN_FONTNAME], FALSE);
    gtk_tree_view_append_column (GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_FONTNAME]);

    renderer = gtk_cell_renderer_text_new();
    appGUI->nte->notes_columns[N_COLUMN_ENCRYPTED] = gtk_tree_view_column_new_with_attributes(NULL,
            renderer,
            "text", N_COLUMN_ENCRYPTED,
            NULL);
    gtk_tree_view_column_set_visible(appGUI->nte->notes_columns[N_COLUMN_ENCRYPTED], FALSE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(appGUI->nte->notes_list), appGUI->nte->notes_columns[N_COLUMN_ENCRYPTED]);

    /* restore columns order */
    columns_order[0] = config.notes_column_idx_0;
    columns_order[1] = config.notes_column_idx_1;
    columns_order[2] = config.notes_column_idx_2;
    columns_order[3] = config.notes_column_idx_3;
    columns_order[4] = config.notes_column_idx_4;

    n = MAX_VISIBLE_NOTE_COLUMNS - 1;

    while (n >= 0) {
        for (i = 0; i < MAX_VISIBLE_NOTE_COLUMNS; i++) {
            if (n == columns_order[i]) {
                gtk_tree_view_move_column_after(GTK_TREE_VIEW(appGUI->nte->notes_list),
                        appGUI->nte->notes_columns[nt_columns[i]], NULL);
                n--;
            }
        }
    }

    set_note_columns_width(appGUI);

    gtk_container_add(GTK_CONTAINER(appGUI->nte->scrolled_win), appGUI->nte->notes_list);

    /* configure sorting */
    for (i = 0; i < MAX_VISIBLE_NOTE_COLUMNS; i++) {
        gtk_tree_view_column_set_sort_column_id(appGUI->nte->notes_columns[nt_columns[i]], nt_columns[i]);
        gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(appGUI->nte->notes_sort), nt_columns[i],
                notes_column_sort_function, GINT_TO_POINTER(nt_columns[i]), NULL);
    }

    /* restore sorting */
    gtk_tree_sortable_set_sort_column_id(GTK_TREE_SORTABLE(appGUI->nte->notes_sort),
            config.notes_sorting_column, config.notes_sorting_order);

    g_signal_connect(appGUI->nte->notes_sort, "sort-column-changed", G_CALLBACK(notes_sort_column_changed_cb), appGUI);

    /*-------------------------------------------------------------------------------------*/
    /* editor */

	vbox3 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    gtk_widget_show (vbox3);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->vbox_editor), vbox3, TRUE, TRUE, 0);
	gtk_widget_set_margin_left(GTK_WIDGET(vbox3), 8);
	gtk_widget_set_margin_right(GTK_WIDGET(vbox3), 8);
	gtk_widget_set_margin_bottom(GTK_WIDGET(vbox3), 8);

    hseparator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (hseparator);
    gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

    appGUI->nte->title_label = gtk_label_new ("");
    gtk_widget_show (appGUI->nte->title_label);
    gtk_label_set_ellipsize (GTK_LABEL(appGUI->nte->title_label), PANGO_ELLIPSIZE_END);
    gtk_box_pack_start (GTK_BOX (vbox3), appGUI->nte->title_label, FALSE, FALSE, 0);

    hseparator = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
    gtk_widget_show (hseparator);
    gtk_box_pack_start (GTK_BOX (vbox3), hseparator, FALSE, TRUE, 4);

    appGUI->nte->editor_viewport = gtk_viewport_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->editor_viewport);
    gtk_viewport_set_shadow_type (GTK_VIEWPORT (appGUI->nte->editor_viewport), GTK_SHADOW_IN);
    gtk_box_pack_start (GTK_BOX (vbox3), appGUI->nte->editor_viewport, TRUE, TRUE, 0);

    appGUI->nte->editor_scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->editor_scrolledwindow);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (appGUI->nte->editor_scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (appGUI->nte->editor_viewport), appGUI->nte->editor_scrolledwindow);

    appGUI->nte->editor_textview = gtk_text_view_new ();
#ifdef HAVE_GSPELL
    appGUI->nte->editor_spelltextview = utl_gui_create_spell_check_textview(GTK_TEXT_VIEW(appGUI->nte->editor_textview), FALSE);
#endif /* HAVE_GSPELL */
    gtk_widget_show (appGUI->nte->editor_textview);
    gtk_container_add (GTK_CONTAINER (appGUI->nte->editor_scrolledwindow), appGUI->nte->editor_textview);
    gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (appGUI->nte->editor_textview), GTK_WRAP_WORD_CHAR);
    gtk_text_view_set_pixels_above_lines(GTK_TEXT_VIEW(appGUI->nte->editor_textview), 4);
    gtk_text_view_set_left_margin(GTK_TEXT_VIEW(appGUI->nte->editor_textview), 8);
    gtk_text_view_set_right_margin(GTK_TEXT_VIEW(appGUI->nte->editor_textview), 8);

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (appGUI->nte->editor_textview));
    appGUI->nte->buffer_check_modify_enable = FALSE;
    g_signal_connect (G_OBJECT (buffer), "changed", G_CALLBACK (text_buffer_modified_cb), appGUI);
    g_signal_connect (G_OBJECT (buffer), "mark_set", G_CALLBACK(editor_cursor_move_cb), appGUI);

    fill_editor_toolbar(appGUI->nte->editor_toolbar, buffer, appGUI);

    gtk_widget_show_all(GTK_WIDGET(appGUI->nte->editor_toolbar));

    /* read-only view */
#ifdef HAVE_LIBWEBKIT

    appGUI->nte->ro_editor_viewport = gtk_viewport_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->ro_editor_viewport);
    gtk_viewport_set_shadow_type (GTK_VIEWPORT (appGUI->nte->ro_editor_viewport), GTK_SHADOW_IN);
    gtk_box_pack_start (GTK_BOX (vbox3), appGUI->nte->ro_editor_viewport, TRUE, TRUE, 0);

    appGUI->nte->ro_editor_scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
    gtk_widget_show (appGUI->nte->ro_editor_scrolledwindow);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (appGUI->nte->ro_editor_scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_container_add (GTK_CONTAINER (appGUI->nte->ro_editor_viewport), appGUI->nte->ro_editor_scrolledwindow);

    appGUI->nte->html_webkitview = utl_create_webkit_web_view (appGUI);

    g_signal_connect (appGUI->nte->html_webkitview, "context-menu",
                                      G_CALLBACK (utl_webkit_on_menu), appGUI);
    g_signal_connect (appGUI->nte->html_webkitview, "navigation-policy-decision-requested",
                                      G_CALLBACK (utl_webkit_link_clicked), appGUI);

    find_controller = webkit_web_view_get_find_controller(appGUI->nte->html_webkitview);
    g_signal_connect (find_controller, "found-text", G_CALLBACK (text_found), appGUI);
    g_signal_connect (find_controller, "failed-to-find-text", G_CALLBACK (text_not_found), appGUI);

	gtk_widget_show (GTK_WIDGET(appGUI->nte->html_webkitview));
    gtk_container_add (GTK_CONTAINER (appGUI->nte->ro_editor_scrolledwindow), 
					   GTK_WIDGET(appGUI->nte->html_webkitview));

	gtk_widget_hide(appGUI->nte->ro_editor_viewport);
#endif  /* HAVE_LIBWEBKIT */

	hbox1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 1);
    gtk_widget_show (hbox1);
    gtk_box_pack_start (GTK_BOX (vbox3), hbox1, FALSE, FALSE, 2);

    appGUI->nte->readonly_checkbutton = gtk_check_button_new_with_mnemonic (_("Read-only"));
    gtk_widget_set_can_focus(appGUI->nte->readonly_checkbutton, FALSE);
    gtk_widget_show (appGUI->nte->readonly_checkbutton);
    gtk_box_pack_end (GTK_BOX (hbox1), appGUI->nte->readonly_checkbutton, FALSE, FALSE, 4);
    g_signal_connect (GTK_TOGGLE_BUTTON(appGUI->nte->readonly_checkbutton), "toggled",
                      G_CALLBACK (readonly_toggle_cb), appGUI);

	appGUI->nte->font_picker = gtk_font_button_new ();
    gtk_widget_show (appGUI->nte->font_picker);
    gtk_widget_set_can_focus(appGUI->nte->font_picker, FALSE);
    g_signal_connect (GTK_FONT_BUTTON(appGUI->nte->font_picker), "font-set",
                      G_CALLBACK (nte_font_selected), appGUI);
    gtk_box_pack_end (GTK_BOX (hbox1), appGUI->nte->font_picker, FALSE, FALSE, 4);

    g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s</b>:", _("Line"));
    appGUI->nte->nrow_label_t = gtk_label_new (tmpbuf);
    gtk_widget_show (appGUI->nte->nrow_label_t);
    gtk_box_pack_start (GTK_BOX (hbox1), appGUI->nte->nrow_label_t, FALSE, FALSE, 0);
    gtk_label_set_use_markup (GTK_LABEL (appGUI->nte->nrow_label_t), TRUE);

    g_snprintf(tmpbuf, BUFFER_SIZE, "<tt>%s</tt>", "1/1");
    appGUI->nte->nrow_label = gtk_label_new (tmpbuf);
    gtk_widget_show (appGUI->nte->nrow_label);
    gtk_box_pack_start (GTK_BOX (hbox1), appGUI->nte->nrow_label, FALSE, FALSE, 8);
    gtk_label_set_use_markup (GTK_LABEL (appGUI->nte->nrow_label), TRUE);

    g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s</b>:", _("Column"));
    appGUI->nte->ncol_label_t = gtk_label_new (tmpbuf);
    gtk_widget_show (appGUI->nte->ncol_label_t);
    gtk_box_pack_start (GTK_BOX (hbox1), appGUI->nte->ncol_label_t, FALSE, FALSE, 8);
    gtk_label_set_use_markup (GTK_LABEL (appGUI->nte->ncol_label_t), TRUE);

    g_snprintf(tmpbuf, BUFFER_SIZE, "<tt>%s</tt>", "1/1");
    appGUI->nte->ncol_label = gtk_label_new (tmpbuf);
    gtk_widget_show (appGUI->nte->ncol_label);
    gtk_box_pack_start (GTK_BOX (hbox1), appGUI->nte->ncol_label, FALSE, FALSE, 0);
    gtk_label_set_use_markup (GTK_LABEL (appGUI->nte->ncol_label), TRUE);

    appGUI->nte->find_hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_widget_show (appGUI->nte->find_hbox);
    gtk_box_pack_start (GTK_BOX (vbox3), appGUI->nte->find_hbox, FALSE, FALSE, 0);

    appGUI->nte->find_next_flag = FALSE;

    g_snprintf(tmpbuf, BUFFER_SIZE, "<b>%s</b>:", _("Find"));
    label = gtk_label_new (tmpbuf);
    gtk_widget_show (label);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->find_hbox), label, FALSE, FALSE, 0);
    gtk_label_set_use_markup (GTK_LABEL (label), TRUE);

    appGUI->nte->find_entry = gtk_entry_new ();
    gtk_widget_show (appGUI->nte->find_entry);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->find_hbox), appGUI->nte->find_entry, TRUE, TRUE, 4);
    g_signal_connect (G_OBJECT (appGUI->nte->find_entry), "key_press_event",
                      G_CALLBACK (find_entry_key_press_cb), appGUI);

    find_backward_button = gtk_button_new_from_icon_name ("go-previous", GTK_ICON_SIZE_BUTTON);
    gtk_widget_set_can_focus(find_backward_button, FALSE);
    gtk_button_set_relief (GTK_BUTTON(find_backward_button), GTK_RELIEF_NONE);
    gtk_widget_show (find_backward_button);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->find_hbox), find_backward_button, FALSE, FALSE, 1);
    g_signal_connect (G_OBJECT (find_backward_button), "clicked",
                      G_CALLBACK (editor_find_backward_cb), appGUI);

	find_forward_button = gtk_button_new_from_icon_name ("go-next", GTK_ICON_SIZE_BUTTON);
    gtk_button_set_relief (GTK_BUTTON(find_forward_button), GTK_RELIEF_NONE);
    gtk_widget_set_can_focus(find_forward_button, FALSE);
    gtk_widget_show (find_forward_button);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->find_hbox), find_forward_button, FALSE, FALSE, 1);
    g_signal_connect (G_OBJECT (find_forward_button), "clicked",
                      G_CALLBACK (editor_find_forward_cb), appGUI);

    appGUI->nte->find_case_checkbutton = gtk_check_button_new_with_mnemonic (_("case sensitive"));
    gtk_widget_set_can_focus(appGUI->nte->find_case_checkbutton, FALSE);
    gtk_widget_show (appGUI->nte->find_case_checkbutton);
    gtk_box_pack_start (GTK_BOX (appGUI->nte->find_hbox), appGUI->nte->find_case_checkbutton, FALSE, FALSE, 4);
    g_signal_connect (GTK_TOGGLE_BUTTON(appGUI->nte->find_case_checkbutton), "toggled",
                      G_CALLBACK (case_sensitive_toggle_cb), appGUI);


    close_button = gtk_button_new_from_icon_name ("window-close", GTK_ICON_SIZE_BUTTON);
    gtk_widget_set_can_focus(close_button, FALSE);
    gtk_button_set_relief (GTK_BUTTON(close_button), GTK_RELIEF_NONE);
	if (config.enable_tooltips) {
		gtk_widget_set_tooltip_text (close_button, _("Close find entry"));
	}
    gtk_box_pack_end (GTK_BOX (appGUI->nte->find_hbox), close_button, FALSE, FALSE, 0);
    g_signal_connect (G_OBJECT (close_button), "clicked",
                      G_CALLBACK (editor_find_text_hide_cb), appGUI);

    gtk_widget_hide (appGUI->nte->find_hbox);

    appGUI->nte->changed = FALSE;
    gtk_widget_set_sensitive(GTK_WIDGET(appGUI->nte->save_button), FALSE);

    /*-------------------------------------------------------------------------------------*/

    notes_show_selector_editor (SELECTOR, appGUI);

    appGUI->nte->filename = NULL;

#ifdef HAVE_LIBGRINGOTTS
    appGUI->nte->context = NULL;
    appGUI->nte->keyholder = NULL;
#endif /* HAVE_LIBGRINGOTTS */

}

/*------------------------------------------------------------------------------*/

gboolean
check_if_encrypted (gchar *filename, GUI *appGUI) {

gchar *contents;
gboolean result = FALSE;

	if (g_file_get_contents (notes_get_full_filename(filename, appGUI), &contents, NULL, NULL) == TRUE) {

		if (contents[0] == 'O' && contents[1] == 'S' && contents[2] == 'M' && g_ascii_isalnum(contents[3])) {
			result = TRUE;
		}

		g_free (contents);
	}

	return result;
}

/*------------------------------------------------------------------------------*/

void
check_notes_type (GUI *appGUI) {

GtkTreeIter iter;
gchar *filename;
GdkPixbuf *image;
gboolean encrypted, has_next;

    has_next = gtk_tree_model_get_iter_first (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter);
    while (has_next) {
        gtk_tree_model_get(GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter,
                N_COLUMN_FILENAME, &filename, -1);

        encrypted = check_if_encrypted(filename, appGUI);
        if (encrypted == TRUE) {
            image = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (), OSMO_STOCK_TYPE_ENCRYPTED, 16, 0, NULL);
        } else {
            image = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (), OSMO_STOCK_TYPE_NORMAL, 16, 0, NULL);
        }

        g_free(filename);
        gtk_list_store_set(appGUI->nte->notes_list_store, &iter, N_COLUMN_TYPE, image, N_COLUMN_ENCRYPTED, encrypted, -1);
        g_object_unref(image);
        has_next = gtk_tree_model_iter_next (GTK_TREE_MODEL(appGUI->nte->notes_list_store), &iter);
    }
}

/*------------------------------------------------------------------------------*/

#endif  /* NOTES_ENABLED */

